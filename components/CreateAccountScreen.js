import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    View,
    Text,
    ToastAndroid,
    KeyboardAvoidingView,
} from 'react-native';
import { Formik } from 'formik';

import CustomTextInput from './form/CustomTextInput'
import CreateAccount from './models/CreateAccount';
import ImageLoader from '../components/image/ImageLoader';
import { createUser } from './services/UserService';

export default class CreateAccountScreen extends React.Component {
    state = {
        first_name: null,
        last_name: null,
        email: null,
        password: null,
    };

    render () {
        return (
            <KeyboardAvoidingView behavior="height">
                <ScrollView 
                    style={styles.container}
                    keyboardDismissMode="on-drag">
                    <View style={styles.imageContainer}>
                        <ImageLoader 
                            source={require('../assets/gluten-free-icon.png')}
                            style={styles.image} />
                    </View>
                    <View style={styles.formContainer}>
                        <Formik
                            initialValues={{ first_name: '', last_name: '', email: '', password: '' }}
                            validationSchema={CreateAccount}
                            onSubmit={(values) => {
                                this.createUser(values);
                            }}
                            render={formikProps => (
                                <React.Fragment>
                                    <CustomTextInput
                                        error={formikProps.errors.first_name}
                                        placeholder="First Name"
                                        onChangeText={formikProps.handleChange('first_name')}
                                        maxLength={30} />
                                    <CustomTextInput
                                        error={formikProps.errors.last_name}
                                        placeholder="Last Name"
                                        onChangeText={formikProps.handleChange('last_name')}
                                        maxLength={30} />
                                    <CustomTextInput
                                        error={formikProps.errors.email}
                                        placeholder="Email"
                                        type="email"
                                        onChangeText={formikProps.handleChange('email')}
                                        maxLength={30} />
                                    <CustomTextInput
                                        error={formikProps.errors.password}
                                        secureTextEntry
                                        placeholder="Password"
                                        type="password"
                                        onChangeText={formikProps.handleChange('password')}
                                        maxLength={20} />
                                    <View style={styles.createAccountButtonContainer}>
                                        <TouchableOpacity 
                                            style={styles.createAccountButton} 
                                            onPress={formikProps.handleSubmit}>
                                                <Text style={styles.createAccountText}>Create Account</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.terms}>
                                        <Text>By signing up you agree terms and conditions.</Text>
                                    </View>
                                </React.Fragment>
                            )}
                        />
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }

    createUser = (values) => {
        const self = this;

        createUser(values)
            .then(function (response) {
                if (response.data._id) {
                    self.props.navigation.navigate('Home')
                } else {
                    if (response.data.error && response.data.error === 'ERROR__EMAIL_ALREADY_EXISTS') {
                        ToastAndroid.showWithGravity(
                            'Email already exists. Please user other, or sign in with that email.',
                            ToastAndroid.SHORT,
                            ToastAndroid.CENTER,
                        );
                    }
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
    },
    imageContainer: {
        marginBottom: 30,
        marginTop: 50,
    },
    image: {
        width: 250,
        height: 200,
        resizeMode: 'stretch',
        alignSelf: 'center',
    },
    formContainer: {
        paddingLeft: 10,
        paddingRight: 10,
    },
    createAccountButtonContainer: {
        paddingTop: 10,
        paddingLeft: 3,
        paddingRight: 5,
    },
    createAccountButton: {
        alignItems: 'center',
        backgroundColor: '#F6AD42',
        borderRadius: 2,
        padding: 15,
    },
    createAccountText: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        textTransform: 'uppercase',
    },
    terms: {
        paddingLeft: 5,
        paddingTop: 5,
    }
});
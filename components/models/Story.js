import * as Yup from 'yup';

const Story = Yup.object().shape({
    description: Yup.string()
      .min(2, 'Too Short!')
      .max(30, 'Too Long!')
      .required('Description is required.'),
  });
  
module.exports = Story;
import * as Yup from 'yup';

const VALIDATE__EMAIL = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const SignIn = Yup.object().shape({
    email: Yup.string()
      .matches(VALIDATE__EMAIL, 'Invalid email.')
      .min(2, 'Too Short!')
      .max(30, 'Too Long!')
      .required('Email is required.'),
    password: Yup.string()
      .min(2, 'Too Short!')
      .max(30, 'Too Long!')
      .required('Password is required.'),
  });
  
module.exports = SignIn;
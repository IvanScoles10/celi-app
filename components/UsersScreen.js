import React from 'react';
import { 
    StyleSheet,
    ScrollView,
    View, 
    ToastAndroid,
    KeyboardAvoidingView,
} from 'react-native';
import {
    Avatar,
    Button,
    Radio,
    RadioGroup,
} from 'react-native-ui-kitten';
import { Formik } from 'formik';
import { connect } from 'react-redux';

import CustomTextInput from './form/CustomTextInput'
import UpdateAccount from './models/UpdateAccount';
import { updateUser } from './services/UserService';

class UsersScreen extends React.Component {

    state = {
        first_name: this.props.fi,
        last_name: '',
        email: '',
        gender: 0,
    };

    render () {
        const {
            first_name,
            last_name,
            email,
            gender,
        } = this.props.user;

        return (
            <KeyboardAvoidingView behavior="height" style={styles.container}>
                <ScrollView 
                    keyboardDismissMode="on-drag">
                    <View>
                        <Avatar
                            style={styles.avatar}
                            size='large'
                            source={{ uri: 'https://images.unsplash.com/photo-1494790108377-be9c29b29330' }}
                        />
                    </View>
                    <View style={styles.formContainer}>
                        <Formik
                            initialValues={{
                                first_name: first_name,
                                last_name: last_name,
                                email: email,
                            }}
                            validationSchema={UpdateAccount}
                            onSubmit={(values) => {
                                this.updateUser(values);
                            }}
                            render={formikProps => (
                                <React.Fragment>
                                    <CustomTextInput
                                        error={formikProps.errors.first_name}
                                        placeholder="First Name"
                                        value={(formikProps.values.first_name) ? formikProps.values.first_name : first_name}
                                        onChangeText={formikProps.handleChange('first_name')}
                                        maxLength={30} />
                                    <CustomTextInput
                                        error={formikProps.errors.last_name}
                                        placeholder="Last Name"
                                        value={(formikProps.values.last_name) ? formikProps.values.last_name : last_name}
                                        onChangeText={formikProps.handleChange('last_name')}
                                        maxLength={30} />
                                    <CustomTextInput
                                        error={formikProps.errors.email}
                                        placeholder="Email"
                                        type="email"
                                        value={(formikProps.values.email) ? formikProps.values.email : email}
                                        onChangeText={formikProps.handleChange('email')}
                                        maxLength={30} />
                                    <RadioGroup
                                        value={(formikProps.values.gender) ? formikProps.values.gender : gender}
                                        onChange={this.onChange}>
                                        <Radio style={styles.radio} text='Male' />
                                        <Radio style={styles.radio} text='Female' />
                                    </RadioGroup>
                                    <View style={styles.updateAccountButtonContainer}>
                                        <Button 
                                            style={styles.updateButton}
                                            appearance="outline"
                                            onPress={formikProps.handleSubmit}>Update</Button>
                                    </View>
                                </React.Fragment>
                            )}
                        />
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }

    updateUser = (values) => {
        const self = this;

        updateUser(values)
            .then(function (response) {
                if (response.data._id) {
                    self.props.navigation.navigate('Home')
                } else {
                    if (response.data.error && response.data.error === 'ERROR__EMAIL_ALREADY_EXISTS') {
                        ToastAndroid.showWithGravity(
                            'Email already exists. Please user other, or sign in with that email.',
                            ToastAndroid.SHORT,
                            ToastAndroid.CENTER,
                        );
                    }
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
        flex: 3,
        marginTop: 30,
    },
    avatar: {
        marginLeft: 10,
        marginRight: 10,
        marginVertical: 8,
    },
    formContainer: {
        paddingLeft: 10,
        paddingRight: 10,
    },
    updateAccountButtonContainer: {
        paddingTop: 10,
        paddingLeft: 3,
        paddingRight: 5,
    },
    updateAccountButton: {
        alignItems: 'center',
        backgroundColor: '#F6AD42',
        borderRadius: 2,
        padding: 15,
    },
    updateAccountText: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        textTransform: 'uppercase',
    },
    radio: {
        marginVertical: 4,
    },
    updateButton: {
        marginVertical: 4,
        marginHorizontal: 4,
    }
});

const mapStateToProps = (state) => {
    return {
        user: state.user.data,
    };
};

export default connect(mapStateToProps, null)(UsersScreen);
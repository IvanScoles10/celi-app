import React from 'react';
import {
    Image,
    StyleSheet, 
    View,
    KeyboardAvoidingView,
    ToastAndroid,
} from 'react-native';
import { Button } from 'react-native-ui-kitten';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import { pick } from 'lodash';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';

import Story from './models/Story';
import CustomTextInput from './form/CustomTextInput'
import { createStory } from './services/StoriesService';
import { getUniqueID, upload } from './services/AwsService';
import ImageLoader from './image/ImageLoader';

class CreateStoryScreen extends React.Component {

    state = {
        image: null,
    };

    componentDidMount() {
        this.getPermissionAsync();
    }

    render() {
        let { image } = this.state;

        return (
            <KeyboardAvoidingView style={{ flex: 1 }} behavior="height">
                <View style={styles.container}>
                    <Formik
                        initialValues={{ description: '' }}
                        validationSchema={Story}
                        onSubmit={(values) => {
                            this.createStory(values);
                        }}
                        render={formikProps => (
                            <React.Fragment>
                                <View style={styles.addImageContainer}>
                                    {!image &&
                                        <ImageLoader 
                                            source={require('../assets/photo-camera.png')}
                                            style={{width: 150, height: 200, resizeMode: 'contain' }} />
                                    }
                                    {image &&
                                        <Image
                                            source={{ uri: image.uri }} 
                                            style={styles.imageUpload}
                                        />
                                    }
                                </View>
                                {image &&
                                    <View style={styles.textInputContainer}>
                                        <CustomTextInput
                                            multiline
                                            numberOfLines={4}
                                            error={formikProps.errors.description}
                                            placeholder="Description"
                                            type="text"
                                            onChangeText={formikProps.handleChange('description')}
                                            maxLength={30}
                                        />
                                    </View>
                                }
                                <View style={styles.addImageButtonContainer}>
                                    <Button 
                                        style={styles.uploadImageButton}
                                        appearance="outline"
                                        onPress={this.pickImage}>Pick an image from your gallery</Button>
                                </View>
                                {image &&
                                    <View style={styles.addButtonContainer}>
                                        <Button 
                                            style={styles.uploadImageButton}
                                            appearance="outline"
                                            onPress={formikProps.handleSubmit}>Create Story</Button>
                                    </View>
                                }
                            </React.Fragment>
                        )}
                    />
                </View>
            </KeyboardAvoidingView>
        );
    }

    createStory = async (values) => {
        const self = this
        const { _id } = this.props.user
        const { image } = this.state
        let imageResponse = { Location: null }
        const uniqueId = getUniqueID()
        
        values = {
            ...values,
            user: pick(this.props.user, [
                '_id',
                'first_name',
                'last_name',
                'email',
            ])
        }

        if (image) {
            try {
                imageResponse = await upload(image, uniqueId, _id)
            } catch (err) {
                console.log('Cannot upload image', err);
            }
        }

        story = {
            ...values,
            images: (image) ? [{
                id: uniqueId,
                key: `stories/${_id}/${uniqueId}.jpg`,
                uri: imageResponse.Location
            }] : []
        }

        createStory(story)
            .then(function (response) {
                if (response.data._id) {
                    self.props.changePage(0);
                } else {
                    if (response.data.error && response.data.error === 'ERROR__CREATE_STORY') {
                        ToastAndroid.showWithGravity(
                            'Cannot create story.',
                            ToastAndroid.SHORT,
                            ToastAndroid.CENTER,
                        );
                    }
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    getPermissionAsync = async () => {
        if (Constants.platform.ios) {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            
            if (status !== 'granted') {
                alert('Sorry, we need camera roll permissions to make this work!');
            }
        }
    }

    pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            base64: true
        });

        if (!result.cancelled) {
            this.setState({ image: result });
        }
    }

};

const styles = StyleSheet.create({
    addImageContainer: {
        alignItems: 'center',
        justifyContent: 'center', 
        paddingLeft: 5,
        paddingRight: 5,
        marginTop: 30,
        height: 200,
    },
    addImageButtonContainer: {
        height: 50,
        marginTop: 10,
        marginLeft: 5,
        marginRight: 5,
    },
    addButtonContainer: {
        height: 50,
        marginTop: 10,
        marginLeft: 5,
        marginRight: 5,
    },
    uploadImageButton: {
        marginVertical: 4,
        marginHorizontal: 4,
    },
    imageUpload: {
        marginBottom: 10,
        height: 200,
        width: 180,
        resizeMode: 'contain',
    },
    textInputContainer: {
        height: 80,
        marginLeft: 5,
        marginRight: 5,
    },
    createStoryButton: {
        alignItems: 'center',
        backgroundColor: '#F6AD42',
        borderRadius: 2,
        padding: 15,
    },
    createStoryText: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        textTransform: 'uppercase',
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.user.data,
    };
};

export default connect(mapStateToProps, null)(CreateStoryScreen);

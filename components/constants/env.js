const endpoints = {
    development: {
        api: 'http://localhost:5000/api/v1'
    },
    production: {
        api: 'https://celiapp-server.herokuapp.com/api/v1'
    }
}

module.exports = endpoints;
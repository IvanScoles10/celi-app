import React from 'react';
import { Animated } from 'react-native';

export default class ImageLoader extends React.Component {
    state = {
        opacity: new Animated.Value(0),
    }

    render() {
        return (
            <Animated.Image
                {...this.props} 
                onLoad={this.onLoad}
                style={[
                    {
                        opacity: this.state.opacity,
                        transform: [{
                            scale: this.state.opacity.interpolate ({
                                inputRange: [0, 1],
                                outputRange: [0.50, 1],
                            })
                        }]
                    },
                    this.props.style
                ]} />
        );
    }

    onLoad = () => {
        Animated.timing(this.state.opacity, {
            toValue: 1,
            duration: 500,
            useNativeDriver: true,
        }).start();
    }
};
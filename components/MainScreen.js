import React from 'react';
import {
    Button,
    StyleSheet,
    View,
} from 'react-native';
import { connect } from 'react-redux';

import ImageLoader from '../components/image/ImageLoader';

class MainScreen extends React.Component {

    componentDidMount () {
        // Get information from the store user
        if (this.props.loggedIn) {
            this.props.navigation.navigate('Home')
        }
    }

    render () {
        return (
            <View style={styles.container}>
                <View style={{ alignItems: 'center' }}>
                    <ImageLoader 
                        source={require('../assets/gluten-free-icon.png')}
                        style={{width: 250, height: 490, resizeMode: 'contain' }} />
                </View>
                <View style={styles.createAccountButton}>
                    <Button
                        onPress={(event) => this.handleOnPressButton(event, 'create_account')}
                        title="Create account"
                        color="#F6AD42"
                        accessibilityLabel="Create account to connect with celi-app"
                    />
                </View>
                <View style={styles.separatorLine} />
                <View style={styles.signInButton}>
                    <Button
                        onPress={(event) => this.handleOnPressButton(event, 'sign_in')}
                        title="Sign In"
                        color="#2E94F6"
                        accessibilityLabel="Sign in on celi-app"
                    />
                </View>
            </View>
        );
    }

    handleOnPressButton (event, type) {
        if (type === 'create_account') {
            this.props.navigation.navigate('CreateAccount')
        } else {
            this.props.navigation.navigate('SignIn')
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
        justifyContent: 'center',
    },
    separatorLine: {
        borderWidth: 0.5,
        borderColor: '#798A92',
        margin: 10,
    },
    createAccountButton: {
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10,
    },
    signInButton: {
        paddingTop: 5,
        paddingLeft: 10,
        paddingRight: 10,
    }
});

const mapStateToProps = (state) => {
    return {
        user: state.user.data,
        loggedIn: state.user.loggedIn,
    };
};

export default connect(mapStateToProps, null)(MainScreen);

const axios = require('axios');
import endpoints from '../constants/env';

const getUser = (value) => {
    // code here
};

const createUser = (data) => {
    return axios.post(
        `${endpoints['production'].api}/users`,
        JSON.stringify(data),
        {
            headers: {
                'Content-Type': 'application/json',
            }
        }
    );
};

const updateUser = (value) => {
    // code here
};

const deleteUser = (value) => {
    // code here
};

const signIn = (data) => {
    return axios.post(
        `${endpoints['production'].api}/sign-in`,
        JSON.stringify(data),
        {
            headers: {
                'Content-Type': 'application/json',
            }
        }
    );
}

module.exports = {
    getUser,
    createUser,
    updateUser,
    deleteUser,
    signIn,
};
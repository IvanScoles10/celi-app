const AWS = require('aws-sdk');
import { encode}  from 'base-64'

const BUCKET = 'celi-app'
const REGION = 'us-east-2'
const ACCESS_KEY = 'AKIA5BFOGCB4UHDPFQU5'
const SECRET_KEY = '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'

AWS.config.update({
    accessKeyId: ACCESS_KEY,
    secretAccessKey: SECRET_KEY,
    region: REGION
});

const s3 = new AWS.S3()

const chr4 = () => {
    return Math.random().toString(16).slice(-4);
}

const encodeImage = (data) => {
    var str = data.reduce((a, b) => {
        return a + String.fromCharCode(b)
    }, '');

    return encode(str).replace(/.{76}(?=.)/g,'$&\n');
}

const getImage = async (key) => {
    return new Promise((resolve, reject) => {
        s3.getObject({ 
            Bucket: BUCKET,
            Key: key
        }, (err, file) => {
            if (err) {
                console.log(err)
                reject(`Error trying to get image data for image: ${image}`)
            }

            resolve("data:image/png;base64," + encodeImage(file.Body));
        });
    });
}

const getUniqueID = () => {
    return chr4() + chr4() +
      '-' + chr4() +
      '-' + chr4() +
      '-' + chr4() +
      '-' + chr4() + chr4() + chr4();
  }

const upload = async (image, uniqueId, userId) => {
    const response = await fetch(image.uri);
    const body = await response.blob();

    return s3.upload({
        Bucket: BUCKET,
        Body: body,
        Key: `stories/${userId}/${uniqueId}.jpg`
    }).promise();
}

const getUrl = (image, userId) => {
    return s3.getSignedUrl('getObject', {
        Bucket: BUCKET,
        Key: `stories/${userId}/${image}.jpg`
    });
}

module.exports = {
    getUrl,
    getImage,
    upload,
    getUniqueID
}
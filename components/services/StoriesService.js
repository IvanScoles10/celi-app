const axios = require('axios');
import endpoints from '../constants/env';

const getAll = (page, pageSize) => {
    return axios.get(
        `${endpoints['production'].api}/stories?page=${page}&pageSize=${pageSize}`,
        {
            headers: {
                'Content-Type': 'application/json',
            }
        }
    );
};

const addLike = (data) => {
    return axios.put(
        `${endpoints['production'].api}/stories/${data._id}/add-like`,
        JSON.stringify(data),
        {
            headers: {
                'Content-Type': 'application/json',
            }
        }
    );
};

const removeLike = (data) => {
    return axios.put(
        `${endpoints['production'].api}/stories/${data._id}/remove-like`,
        JSON.stringify(data),
        {
            headers: {
                'Content-Type': 'application/json',
            }
        }
    );
};

const createStory = (data) => {
    return axios.post(
        `${endpoints['production'].api}/stories`,
        JSON.stringify(data),
        {
            headers: {
                'Content-Type': 'application/json',
            }
        }
    );
};

const updateStory = (data) => {
    return axios.put(
        `${endpoints['production'].api}/stories`,
        JSON.stringify(data),
        {
            headers: {
                'Content-Type': 'application/json',
            }
        }
    );
};


const deleteStory = (_id) => {
    return axios.delete(
        `${endpoints['production'].api}/stories/${_id}`,
        {
            headers: {
                'Content-Type': 'application/json',
            }
        }
    );
};

module.exports = {
    addLike,
    removeLike,
    getAll,
    createStory,
    updateStory,
    deleteStory
};
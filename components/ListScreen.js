import React from 'react';
import {
    Image,
    StyleSheet,
    ScrollView,
    ToastAndroid,
    TouchableOpacity,
} from 'react-native';
import { List, ListItem } from 'react-native-ui-kitten';
import { 
    find, 
    filter,
    pick,
    pickBy,
} from 'lodash'
import { connect } from 'react-redux';

import {
    addLike,
    removeLike,
    getAll
} from './services/StoriesService';
import ImageLoader from '../components/image/ImageLoader';
import { getImage } from './services/AwsService';

class ListScreen extends React.Component {
    
    state = {
        page: 1,
        pageSize: 10,
    }

    componentDidMount() {
        const { page, pageSize } = this.state;
        var self = this;

        getAll(page, pageSize)
            .then(function (response) {
                if (response.data) {
                    self.props.getAll(response.data);

                    self.getImageData(response.data);

                } else {
                    ToastAndroid.showWithGravity(
                        'Cannot retrieve stories',
                        ToastAndroid.CENTER,
                    );
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    getImageData = async (data) => {
        var self = this;

        if (data) {
            data.forEach(async (item) => {
                item.images.forEach(async (image) => {
                    const imageData = await getImage(image.key)

                    self.props.loadImage({
                        story: {
                            _id: item._id,
                        },
                        image: {
                            ...image,
                            data: imageData
                        }
                    })
                })
            })
        }
    }

    render() {
        const data = filter(this.props.data, item => pickBy(item,
            '_id',
            'description',
            'user',
            'created_at',
            'updated_at',
        ));

        return (
            <ScrollView
                style={styles.container}
                onScroll={({ nativeEvent }) => {
                    if (this.isCloseToBottom(nativeEvent)) {
                        this.handleMoreItems();
                    }
                }}
                scrollEventThrottle={400}>
                <List
                    data={data}
                    renderItem={this.renderItem}
                />
            </ScrollView>
        );
    }

    renderItem = ({ item, index }) => {
        const { first_name, last_name } = item.user;
        const { _id, description, images } = item;
        const heartSource = (this.hasLikedBefore(_id, this.props.user._id)) ? 
            require('../assets/icons-heart-like.png') : require('../assets/icons-heart.png')

        return (
            <React.Fragment>
                <ListItem
                    title={`${first_name}, ${last_name}`}
                    description={description} />
                    {
                        (images && images[_id]) ? <ImageLoader 
                            source={{ uri: images[_id][0].data }}
                            style={{width: '100%', height: 150, resizeMode: 'cover' }} />  : null
                    }
                    <TouchableOpacity style={styles.actionMenu} onPress={(e) => this.handleLiked(e, _id)}>  
                        <Image 
                            style={{width: 32 , height: 32 ,}}
                            source={heartSource}
                        />
                    </TouchableOpacity>
            </React.Fragment>
        );
    }

    handleLiked = (event, _id) => {
        event.preventDefault();
        const user_id = this.props.user._id;
        const self = this;

        // change this to the current user
        if (this.hasLikedBefore(_id, user_id)) {
            removeLike({
                _id,
                user: pick(self.props.user, [
                    '_id',
                    'first_name',
                    'last_name'
                ])
            }).then(function (response) {
                if (response.data) {
                    self.props.removeLike({
                        user: pick(self.props.user, [
                            '_id',
                            'first_name',
                            'last_name'
                        ]),
                        story: {
                            _id
                        }
                    });
                } else {
                    ToastAndroid.showWithGravity(
                        'Cannot add like',
                        ToastAndroid.CENTER,
                    );
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        } else {
            addLike({
                _id,
                user: pick(self.props.user, [
                    '_id',
                    'first_name',
                    'last_name'
                ])
            }).then(function (response) {
                if (response.data) {
                    self.props.addLike({
                        user: pick(self.props.user, [
                            '_id',
                            'first_name',
                            'last_name'
                        ]),
                        story: {
                            _id
                        }
                    });
                } else {
                    ToastAndroid.showWithGravity(
                        'Cannot add like',
                        ToastAndroid.CENTER,
                    );
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        }
    }

    hasLikedBefore = (_id, user_id) => {
        return find(this.props.likes[_id], { _id: user_id }) 
    }

    isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
        const paddingToBottom = 20;
        return layoutMeasurement.height + contentOffset.y >=
          contentSize.height - paddingToBottom;
    }

    handleMoreItems = () => {
        const self = this;
        const { page, pageSize } = this.state;

        this.setState({
            page: page + 1,
            pageSize: pageSize,
        }, () => {
            getAll(this.state.page, this.state.pageSize)
                .then(function (response) {
                    if (response.data) {
                        self.props.findMore(response.data);
                    } else {
                        ToastAndroid.showWithGravity(
                            'Cannot retrieve stories',
                            ToastAndroid.CENTER,
                        );
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        });
    }
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
        flex: 3,
        marginTop: 30,
    },
    actionMenu: {
        backgroundColor: '#FFF',
        paddingTop: 10,
        paddingLeft: 15,
    }
});

const mapStateToProps = (state) => {
    return {
        user: state.user.data,
        data: state.stories.data,
        likes: state.stories.likes,
        images: state.stories.images,
    };
};

const mapDispatchToProps = (dispatch) => {
    // Action
    return {
        // Get all stories
        getAll: (payload) => dispatch({
            type: 'STORIES__GET_ALL',
            payload,
        }),

        loadImage: (payload) => dispatch({
            type: 'IMAGE__LOAD',
            payload,
        }),

        findMore: (payload) => dispatch({
            type: 'STORIES__FIND_MORE',
            payload,
        }),

        addLike: (payload) => dispatch({
            type: 'STORIES__ADD_LIKE',
            payload,
        }),

        removeLike: (payload) => dispatch({
            type: 'STORIES__REMOVE_LIKE',
            payload,
        }),
    };
  };

  export default connect(mapStateToProps, mapDispatchToProps)(ListScreen);

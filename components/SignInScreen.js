import React from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    ToastAndroid,
    TouchableOpacity,
    KeyboardAvoidingView,
} from 'react-native';
import { Formik } from 'formik';
import { connect } from 'react-redux';

import CustomTextInput from './form/CustomTextInput'
import ImageLoader from '../components/image/ImageLoader';
import SignIn from './models/SignIn';
import { signIn } from './services/UserService';

class SignInScreen extends React.Component {

    render () {
        return (
            <KeyboardAvoidingView style={{ flex: 1 }} behavior="height">
                <ScrollView 
                    keyboardDismissMode="on-drag">
                    <View style={styles.imageContainer}>
                        <ImageLoader 
                            source={require('../assets/gluten-free-icon.png')}
                            style={{width: 250, height: 200, resizeMode: 'stretch'}} />
                    </View>
                    <View style={styles.formContainer}>
                        <Formik
                            initialValues={{ email: '', password: '' }}
                            validationSchema={SignIn}
                            onSubmit={(values) => {
                                this.signIn(values);
                            }}
                            render={formikProps => (
                                <React.Fragment>
                                    <CustomTextInput
                                        error={formikProps.errors.email}
                                        placeholder="Email"
                                        type="email"
                                        onChangeText={formikProps.handleChange('email')}
                                        maxLength={30} />
                                    <CustomTextInput
                                        error={formikProps.errors.password}
                                        secureTextEntry
                                        placeholder="Password"
                                        type="password"
                                        onChangeText={formikProps.handleChange('password')}
                                        maxLength={20} />
                                    <View style={styles.signInButtonContainer}>
                                        <TouchableOpacity 
                                            style={styles.signInButton} 
                                            onPress={formikProps.handleSubmit}>
                                                <Text style={styles.signInText}>Sign In</Text>
                                        </TouchableOpacity>
                                    </View>
                                </React.Fragment>
                            )}
                        />
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }

    signIn = (values) => {
        const self = this;

        signIn(values)
            .then(function (response) {
                if (response.data._id) {
                    self.props.signIn({ user: response.data });
                    self.props.navigation.navigate('Home')
                } else {
                    if (response.data.error && response.data.error === 'ERROR__INVALID_EMAIL_OR_PASSWORD') {
                        ToastAndroid.showWithGravity(
                            'Invalid email or password, please try again.',
                            ToastAndroid.SHORT,
                            ToastAndroid.CENTER,
                        );
                    }
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    imageContainer: {
        alignItems: 'center',
        marginBottom: 50,
        marginTop: 50,
    },
    formContainer: {
        backgroundColor: '#FFF',
        paddingLeft: 10,
        paddingRight: 10,
    },
    signInButtonContainer: {
        paddingLeft: 3,
        paddingRight: 5,
        marginTop: 10,
    },
    signInButton: {
        alignItems: 'center',
        backgroundColor: '#F6AD42',
        borderRadius: 2,
        padding: 15,
    },
    signInText: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        textTransform: 'uppercase',
    },
});

const mapDispatchToProps = (dispatch) => {
    // Action
    return {
        // SignIn user
        signIn: (payload) => dispatch({
            type: 'USER__SIGN_IN',
            payload,
        }),
    };
  };

  export default connect(null, mapDispatchToProps)(SignInScreen);

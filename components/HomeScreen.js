import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { BottomNavigation, BottomNavigationTab, Header } from 'react-native-ui-kitten';
import { FlatHeader, Group } from 'react-native-flat-header';
import Icon from 'react-native-vector-icons/FontAwesome';

import ListScreen from './ListScreen';
import UsersScreen from './UsersScreen';
import SettingsScreen from './SettingsScreen';
import CreateStoryScreen from './CreateStoryScreen';

export default class HomeScreen extends React.Component {
    
    constructor(props) {
        super(props)
    }

    state = {
        selectedIndex: 0,
    };

    render() {
        const self = this;
        return (
            <View style={styles.container}>
                <View style={styles.topNavigation}>
                    <FlatHeader
                        leftIcon={<Icon name="map-marker" size={30} color="#FFF" />}
                        leftIconHandler={() => {
                            console.warn('Icon Pressed');
                        }}
                        leftText="Celi App"
                        leftTextHandler={() => {}}
                        large
                        style={{ backgroundColor: '#EEB942' }}
                    />
                </View>
                {this.renderSelected()}
                <View style={styles.bottomNavigation}>
                    <BottomNavigation
                        selectedIndex={this.state.selectedIndex}
                        onSelect={this.onTabSelect}>
                        <BottomNavigationTab icon={this.renderHomeIcon} />
                        <BottomNavigationTab icon={this.renderPlusIcon} />
                        <BottomNavigationTab icon={this.renderUsersIcon} />
                        <BottomNavigationTab icon={this.renderSettingsIcon} />
                    </BottomNavigation>
                </View>
            </View>
        );
    }

    renderSelected = () => {
        const { selectedIndex } = this.state;
        let view = null;

        if (selectedIndex === 0) {
            view = <ListScreen />
        } else if (selectedIndex === 1) {
            view = <CreateStoryScreen changePage={this.changePage} />
        } else if (selectedIndex === 2) {
            view = <UsersScreen />
        } else if (selectedIndex === 3) {
            view = <SettingsScreen />
        }

        return view;
    }

    changePage = (pageIndex) => {
        this.setState({ selectedIndex: pageIndex });
    }

    onTabSelect = (selectedIndex) => {
        this.setState({ selectedIndex });
    };

    renderHomeIcon = () => {
        return (
            <Image
                style={{ width: 30, height: 30, resizeMode: 'contain' }}
                source={require(`../assets/organic-icon.png`)}
            />
        );
    }

    renderUsersIcon = () => {
        return (
            <Image
                style={{ width: 30, height: 30, resizeMode: 'contain' }}
                source={require(`../assets/icons-users.png`)}
            />
        );
    }

    renderSettingsIcon = () => {
        return (
            <Image
                style={{ width: 30, height: 30, resizeMode: 'contain' }}
                source={require(`../assets/icons-settings.png`)}
            />
        );
    }

    renderPlusIcon = () => {
        return (
            <Image
                style={{ width: 30, height: 30, resizeMode: 'contain' }}
                source={require(`../assets/icons-plus.png`)}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    navigation: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    topNavigation: {
        paddingTop: 23,
        height: 45,
    },
    bottomNavigation: {
        justifyContent: 'flex-end',
    }
});

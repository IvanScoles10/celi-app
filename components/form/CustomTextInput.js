import React from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
} from 'react-native';

const COLOR_BLUE = '#428AF8';
const COLOR_LIGHT_GRAY = '#D3D3D3';

export default class CustomInputText extends React.Component {
    state = {
        isFocused: false
    };

    render () {
        const { isFocused } = this.state;
        const { error } = this.props;

        return (
            <React.Fragment>
                <TextInput
                    style={styles.textInput}
                    selectionColor={COLOR_BLUE}
                    underlineColorAndroid={
                        isFocused ? COLOR_BLUE : COLOR_LIGHT_GRAY
                    }
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    {...this.props} />
                {(error) ? <Text style={styles.textError}>{error}</Text> : null}
            </React.Fragment>
        );
    }
}

const styles = StyleSheet.create({
    textInput: {
        height: 55,
        paddingLeft: 5,
        fontSize: 20,
        borderColor: '#CCCCCC',
    },
    textError: {
        color: '#DD342F',
        paddingLeft: 5,
    }
});
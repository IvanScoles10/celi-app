import { combineReducers } from 'redux';
import { findIndex, cloneDeep, remove } from 'lodash';

import { getImage } from '../services/AwsService';

// data
// "likes": [],
// "_id": "5d96da997bbab14792f1406c",
// "description": "Conoce todas nuestras delicias, somos unos de los pocos locales en el centro de cordoba.",
// "user": {
//     "_id": "5d7c615d1701125a0717f014",
//     "first_name": "Oriana",
//     "last_name": "Dinarelli",
//     "email": "ori.dinareli@gmail.com",
//      "password": "$2b$10$rKjhJACrS0rJL9K5NBSM4OZT49Cb4QvwNfh4Cr1FCajEkkpB./0DG"
// },
// "created_at": "2019-10-04T05:37:29.488Z",
// "updated_at": "2019-10-04T05:37:29.489Z",
// "__v": 0
const INITIAL_STATE = {
    data: {},
    likes: {},
    images: {}
};

const addImageToStory = async (images) => {
    const data = images.map(async (item)  => {
        const imageData = await getImage(item.key)

        return {
            ...item,
            data: imageData,
        }
    });

    return data;
}

const StoriesReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'STORIES__GET_ALL': {
            const data = action.payload.reduce((story, it) => (story[it._id] = it, story), {})
            const likes = action.payload.reduce((story, it) => (story[it._id] = it.likes, story), {});
            const images = action.payload.reduce((story, it) => (story[it._id] = it.images, story), {});
            
            return {
                data,
                likes,
                images,
            }
        }

        case 'STORIES__FIND_MORE': {
            const data = action.payload.reduce((story, it) => (story[it._id] = it, story), {})
            let likes = action.payload.reduce((story, it) => (story[it._id] = it.likes, story), {});

            return {
                data: {
                    ...state.data,
                    ...data
                },
                likes: {
                    ...state.likes,
                    ...likes,
                }
            }

        }

        case 'STORIES__ADD_LIKE': {
            const likes = {
                ...state.likes,
                [action.payload.story._id]: [
                    ...[action.payload.story._id],
                    action.payload.user
                ],
            };

            return {
                data: state.data,
                likes,
            };
        }

        case 'STORIES__REMOVE_LIKE': {
            let likes = cloneDeep(state.likes);
            
            likes[action.payload.story._id] = remove(
                likes[action.payload.story._id], like => like._id === action.payload.story._id
            );

            return {
                data: state.data,
                likes,
            }
        }

        case 'IMAGE__LOAD': {
            let images = cloneDeep(state.images)
            
            let data = images[action.payload.story._id];
            const imageIndex = findIndex(data, image => image.id = action.payload.image.id);
            data[imageIndex] = {
                ...action.payload.image
            };
            images[action.payload.story._id] = data;
            state.data[action.payload.story._id].images = images;

            return {
                data: state.data,
                likes: state.likes,
                images
            }
        }

        default:
            return state
    }
};

export default StoriesReducer;
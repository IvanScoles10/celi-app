import { combineReducers } from 'redux';

import UserReducer from './UserReducer';
import StoriesReducer from './StoriesReducer';

export default combineReducers({
    user: UserReducer,
    stories: StoriesReducer,
});
import { AsyncStorage } from 'react-native';
import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';

import reducers from './reducers';

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whitelist: [
        'user',
    ],
    blacklist: [
        'stories',
    ],
};

const persistedReducer = persistReducer(persistConfig, reducers);

const store = createStore(
    persistedReducer
);
let persistor = persistStore(store);

export {
    store,
    persistor,
};
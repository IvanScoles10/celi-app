// data
// {
//     "_id": "5d7c615d1701125a0717f014",
//     "first_name": "Oriana",
//     "last_name": "Dinarelli",
//     "email": "ori.dinareli@gmail.com",
//     "password": "$2b$10$rKjhJACrS0rJL9K5NBSM4OZT49Cb4QvwNfh4Cr1FCajEkkpB./0DG",
//     "created_at": "2019-09-14T03:41:17.272Z",
//     "updated_at": "2019-09-14T03:41:17.280Z",
//     "__v": 0
// },
const INITIAL_STATE = {
    user: {},
    loggedIn: false,
};

const UserReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case 'USER__SIGN_IN': {
            return {
                data: action.payload.user,
                loggedIn: true,
            }
        }

        default:
            return state
    }
};

export default UserReducer;
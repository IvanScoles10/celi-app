import React from 'react';

import { createDrawerNavigator, createAppContainer } from "react-navigation";
import { mapping, light as lightTheme } from '@eva-design/eva';
import { ApplicationProvider } from 'react-native-ui-kitten';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './components/reducers/store';

import MainScreen from './components/MainScreen';
import HomeScreen from './components/HomeScreen';
import CreateAccountScreen from './components/CreateAccountScreen'
import SignInScreen from './components/SignInScreen'

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <ApplicationProvider
                        mapping={mapping}
                        theme={lightTheme}>
                        <AppContainer />
                    </ApplicationProvider>
                </PersistGate>
            </Provider>
        )
    }
}

const AppNavigator = createDrawerNavigator({
    Main: {
        screen: MainScreen
    },
    Home: {
        screen: HomeScreen
    },
    CreateAccount: {
        screen: CreateAccountScreen
    },
    SignIn: {
        screen: SignInScreen
    }
}, {
    initialRouteName: "Main"
});

const AppContainer = createAppContainer(AppNavigator);
